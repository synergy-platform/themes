<?php

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/shanedaniels/Themes
 */

return [
	'exception' => [
		'no_active_theme' => 'An active theme has not been set.',
		'theme_not_registered' => 'The theme :slug has is not been registered.'
	]
];