<?php

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/themes
 */

if ( ! function_exists("themes"))
{
	/**
	 * Returns a new instance of our assets class
	 *
	 * @return mixed
	 */
	function themes()
	{
		return app()['themes'];
	}
}

if ( ! function_exists("assets"))
{
	/**
	 * Returns a new instance of our assets class
	 *
	 * @return mixed
	 */
	function assets()
	{
		return app()['assets'];
	}
}

if ( ! function_exists('asset_queue'))
{
	/**
	 * Queues a new asset
	 *
	 * @param $slug
	 * @param $key
	 * @param array $dependencies
	 * @param array $attributes
	 */
	function asset_queue($slug, $key, array $dependencies = [], array $attributes = [])
	{
		app()['assets']->queue($slug, $key, $dependencies, $attributes);
	}
}

if ( ! function_exists('getUrl'))
{
	/**
	 * Returns the url to a theme asset
	 *
	 * @param $url
	 * @return
	 */
	function getUrl($url)
	{
		return assets()->getUrl($url);
	}
}

if ( ! function_exists('getCompiledScripts'))
{
	/**
	 * Returns our compiled scripts
	 */
	function getCompiledScripts()
	{
		foreach (assets()->getCompiledScripts() as $script)
		{
			echo "<script src=\"{$script}\"></script>";
		}
	}
}

if ( ! function_exists('getCompiledStyles'))
{
	/**
	 * Returns our compiled styles
	 */
	function getCompiledStyles()
	{
		foreach (assets()->getCompiledStyles() as $style)
		{
			echo "<link rel=\"stylesheet\" href=\"{$style}\"/>";
		}
	}
}