<?php

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/themes
 */

return [

	/*
	|--------------------------------------------------------------------------
	| Enabled
	|--------------------------------------------------------------------------
	|
	| Is the theme package enabled. This is helpful when you want to manually
	| install or configure the package via an installer.
	|
	*/
	'enabled' => false,

	/*
	|--------------------------------------------------------------------------
	| Paths
	|--------------------------------------------------------------------------
	|
	| Path to directories containing our themes
	|
	*/
	'paths' => [
		public_path('themes')
	],

	/*
    |--------------------------------------------------------------------------
    | Manifest
    |--------------------------------------------------------------------------
    |
    | Specify the name of the module and the depth that the finder will search
	| when looking inside the module folders for the manifest file.
    |
    */
	'manifest' => [

		/*
		 * Name of the manifest file
		 */
		'name' => 'theme.php',

		/*
		 * Depth that the finder will look inside directories for
		 * the manifest file.
		 */
		'depth' => 2,

		/*
		 * Rules used to validate the module's manifest file
		 */
		'rules' => [
			'slug' => 'required',
			'name' => 'required',
			'description' => 'required',
			'version' => 'required',
			'author.name' => 'required',
			'author.email' => 'required'
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Theme Class
	|--------------------------------------------------------------------------
	|
	| Name of the class used to create new theme instances
	|
	*/
	'themeClass' => '\Synergy\Themes\Theme',

	/*
	|--------------------------------------------------------------------------
	| Active
	|--------------------------------------------------------------------------
	|
	| Slug of the active theme
	|
	*/
	'active' => 'frontend::default',

	/*
	|--------------------------------------------------------------------------
	| Fallback Theme
	|--------------------------------------------------------------------------
	|
	| Slug for the fallback theme to use
	|
	*/
	'fallback' => null,

	/*
	|--------------------------------------------------------------------------
	| Resources
	|--------------------------------------------------------------------------
	|
	| Locations of resources relative to the theme or app's base path.
	|
	*/
	'resources' => [
		'assets' => 'assets',
		'views' => 'resources/views',
		'vendor' => 'vendor'
	],

	/*
	|--------------------------------------------------------------------------
	| Asset Configuration
	|--------------------------------------------------------------------------
	|
	| Locations of resources relative to the theme or app's base path.
	|
	*/
	'assets' => [
		/*
    	|--------------------------------------------------------------------------
    	| Source Path
    	|--------------------------------------------------------------------------
    	|
    	| Path to your assets source files.
    	|
    	*/
		'sourcePath' => public_path('assets'),

		/*
		|--------------------------------------------------------------------------
		| Public Path
		|--------------------------------------------------------------------------
		|
		| Base path to the public root.
		|
		*/
		'publicPath' => public_path(),

		/*
		|--------------------------------------------------------------------------
		| Public Path
		|--------------------------------------------------------------------------
		|
		| Path to the public directory where your assets will be stored.
		|
		*/
		'compiledPath' => public_path('assets/compiled'),

		/*
		|--------------------------------------------------------------------------
		| Debug Mode
		|--------------------------------------------------------------------------
		|
		| If set to true assets will be compiled individually rather than together
		| as a collection.
		|
		*/
		'debug' => false,

		/*
		|--------------------------------------------------------------------------
		| Force Compile
		|--------------------------------------------------------------------------
		|
		| When set to true, all assets will be recompiled.
		|
		*/
		'forceCompile' => false,

		/*
		|--------------------------------------------------------------------------
		| Style Extensions
		|--------------------------------------------------------------------------
		|
		| Array of extensions that will be matched to the "styles" asset type.
		|
		*/
		'styleExtensions' => [
			'css',
			'less',
			'sass',
			'scss'
		],

		/*
		|--------------------------------------------------------------------------
		| Script Extensions
		|--------------------------------------------------------------------------
		|
		| Array of extensions that will me matched to the "scripts" asset type
		|
		*/
		'scriptExtensions' => [
			'js',
			'coffee'
		],

		/*
		|--------------------------------------------------------------------------
		| Compiled Styles Filename
		|--------------------------------------------------------------------------
		|
		| The name that will be used for the non-debugged styles collection.
		|
		*/
		'compiledStylesFilename' => 'styles',

		/*
		|--------------------------------------------------------------------------
		| Compiled Scripts Filename
		|--------------------------------------------------------------------------
		|
		| The name that will be used for the non-debugged styles collection.
		|
		*/
		'compiledScriptsFilename' => 'scripts',

		/*
		|--------------------------------------------------------------------------
		| Filters
		|--------------------------------------------------------------------------
		|
		| Filters to be applied based on the asset type.
		|
		*/
		'filters' => [

			'css' => [
				'Assetic\Filter\CssImportFilter',
				'Synergy\Themes\Assets\Filters\UriRewriter'
			],

			'js' => [
				'Assetic\Filter\JSMinFilter'
			],

			'less' => [
				'Synergy\Themes\Assets\Filters\LessPhp'
			]
		]
	]
];
