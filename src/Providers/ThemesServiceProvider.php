<?php

namespace Synergy\Themes\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\ViewServiceProvider;
use Symfony\Component\Finder\Finder as SymfonyFinder;
use Synergy\Themes\Assets\Assets;
use Synergy\Themes\Finder;
use Synergy\Themes\Themes;
use Synergy\Themes\ViewFinder;

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/themes
 */

class ThemesServiceProvider extends ServiceProvider
{

	/**
	 * Boots the package's resources
	 */
	public function boot()
	{
		$this->bootResources();

		$this->overrideViewFinder();

		$this->app['themes']->findAndRegister();

		$config = config('synergy-themes');

		if ($active = $config['active']) {
			$this->app['themes']->setActive($active);
		}

		if ($fallback = $config['fallback']) {
			$this->app['themes']->setFallback($fallback);
		}
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerThemeFinder();
		$this->registerThemes();
		$this->registerAssets();

		$this->commands([
			'Synergy\Themes\Console\Commands\PublishTheme'
		]);
	}


	/**
	 * Registers Theme Finder instance with the IoC
	 */
	protected function registerThemeFinder()
	{
		$this->app->singleton('themes.finder', function($app) {

			$paths = config('synergy-themes.paths');

			return new Finder(new SymfonyFinder, $paths);
		});
	}

	protected function registerThemes()
	{
		$this->app->singleton('themes', function($app) {

			$themes = new Themes;

			$themes->setValidationRules(config('synergy-themes.manifest.rules'));
			$themes->setThemeClass(config('synergy-themes.themeClass'));

			return $themes;
		});
	}

	/**
	 * Override the view finder used by Laravel to be our Theme view finder.
	 */
	protected function overrideViewFinder()
	{
		$originalViewFinder = $this->app['view.finder'];

		$this->app['view.finder'] = $this->app->share(function ($app) use ($originalViewFinder) {
			$paths = array_merge(
				$app['config']['view.paths'],
				$originalViewFinder->getPaths()
			);

			$viewFinder = new ViewFinder($app['files'], $paths, $originalViewFinder->getExtensions());
			$viewFinder->setThemes($app['themes']);

			foreach ($originalViewFinder->getPaths() as $location) {
				$viewFinder->addLocation($location);
			}

			foreach ($originalViewFinder->getHints() as $namespace => $hints) {
				$viewFinder->addNamespace($namespace, $hints);
			}

			return $viewFinder;
		});

		// Now that we have overridden the "view.finder" IoC offest, we
		// need to re-register the factory as we cannot reset it
		// on the Factory at runtime, yet.
		$viewServiceProvider = new ViewServiceProvider($this->app);
		$viewServiceProvider->registerFactory();
	}


	/**
	 * Registers the Asset Manager with the IoC
	 */
	protected function registerAssets()
	{
		$this->app->singleton('assets', function($app) {

			$assets = new Assets($app['themes'], $app['files']);

			$config = $app['config']['synergy-themes.assets'];

			$assets->setDebug($this->setDebug($config['debug']));
			$assets->setForceCompile($config['forceCompile']);
			$assets->setPublicPath($config['publicPath']);
			$assets->setSourcePath($config['sourcePath']);
			$assets->setCompiledPath($config['compiledPath']);
			$assets->setCompiledScriptsFilename($config['compiledScriptsFilename']);
			$assets->setCompiledStylesFilename($config['compiledStylesFilename']);
			$assets->setFilters($config['filters']);
			$assets->setScriptExtensions($config['scriptExtensions']);
			$assets->setStyleExtensions($config['styleExtensions']);

			return $assets;
		});
	}

	/**
	 * Sets our debug status.
	 *
	 * If a debug is passed from the config, we will use the
	 * value provided. If the config value is set to null,
	 * we will attempt to get the current environment.
	 *
	 * @param null $debug
	 * @return bool|null
	 */
	protected function setDebug($debug = null)
	{
		if ( is_null($debug))
		{
			if ( $this->app->environment('production')) {
				return false;
			}

			return true;
		}

		return $debug;
	}

	/**
	 * Boots the packages resources (translations, config, views, etc...)
	 */
	protected function bootResources()
	{
		// Publish config
		$config = realpath(__DIR__ . '/../../config/config.php');

		$this->mergeConfigFrom($config, 'synergy-themes');

		$this->publishes([
			$config => config_path('synergy-themes.php'),
		], 'config');

		$this->loadTranslationsFrom(__DIR__ . '/../../lang', 'synergy-themes');

	}

}