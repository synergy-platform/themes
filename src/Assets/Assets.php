<?php

namespace Synergy\Themes\Assets;

use Assetic\Asset\AssetCollection;
use Assetic\Asset\AssetInterface;
use Assetic\AssetWriter;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\NamespacedItemResolver;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Synergy\Dependable\Sorter;
use Synergy\Themes\Themes;
use URL;

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/themes
 */

class Assets {

	/**
	 * Holds our queued styles
	 *
	 * @var array
	 */
	protected $styles = [];

	/**
	 * Holds our queued scripts
	 *
	 * @var array
	 */
	protected $scripts = [];

	/**
	 * Path to our assets source folder
	 *
	 * @var string
	 */
	protected $sourcePath;

	/**
	 * Path to where we are storing our compiled
	 * assets
	 *
	 * @var string
	 */
	protected $compiledPath;

	/**
	 * Path to the public assets root dir
	 *
	 * @var string
	 */
	protected $publicPath;

	/**
	 * Array containing extensions that will be mapped
	 * to the "styles" type.
	 *
	 * @var array
	 */
	protected $styleExtensions = [];

	/**
	 * Array containing extensions that will be mapped
	 * to the "scripts" type.
	 *
	 * @var array
	 */
	protected $scriptExtensions = [];

	/**
	 * Name of the compiled scripts file when
	 * we are not in debug mode.
	 *
	 * @var string
	 */
	protected $compiledStylesFilename;

	/**
	 * Name of the compiled scripts file when
	 * we are not in debug mode.
	 *
	 * @var string
	 */
	protected $compiledScriptsFilename;

	/**
	 * Array containing our filters
	 *
	 * @var array
	 */
	protected $filters = [];

	/**
	 * Flag for debug mode
	 *
	 * @var bool
	 */
	protected $debug = false;

	/**
	 * If true, we will force all assets to be
	 * recompiled.
	 *
	 * @var bool
	 */
	protected $forceCompile = false;

	/**
	 * @var Themes
	 */
	protected $themes;

	/**
	 * Creates a new instance of our Asset Manager
	 *
	 * @param Themes $themes
	 * @param Filesystem $filesystem
	 */
	public function __construct(Themes $themes, Filesystem $filesystem)
	{
		$this->filesystem = $filesystem;

		$this->themes = $themes;
	}


	/**
	 * Queues a new asset to be added to the collection
	 *
	 * @param $key
	 * @param $path
	 * @param array $dependencies
	 */
	public function queue($key, $path, array $dependencies = [])
	{
		$type = $this->getAssetTypeFromExtension($path);

		$method = 'add' . Str::singular(studly_case($type));

		$this->$method($key, $path, $dependencies);
	}

	/**
	 * Adds a new style to the collection
	 *
	 * @param $key
	 * @param $path
	 * @param array $dependencies
	 */
	public function addStyle($key, $path, array $dependencies = [])
	{
		$asset = $this->createAssetInstance($key, $path, $dependencies);

		$this->addItem('styles', $asset);
	}

	/**
	 * Adds a new script to the collection
	 *
	 * @param $key
	 * @param $path
	 * @param array $dependencies
	 */
	public function addScript($key, $path, array $dependencies = [])
	{
		$asset = $this->createAssetInstance($key, $path, $dependencies);

		$this->addItem('scripts', $asset);
	}

	/**
	 * Adds an item to the collection
	 *
	 * @param $type
	 * @param $asset
	 */
	public function addItem($type, $asset)
	{
		$this->{$type}[] = $asset;
	}

	/**
	 * Creates an instance of our Asset
	 *
	 * @param $key
	 * @param $path
	 * @param array $dependencies
	 * @return mixed
	 */
	protected function createAssetInstance($key, $path, array $dependencies = [])
	{
		$path = $this->getPath($path);

		$class = $this->getAssetClass($path);

		$asset = new $class($path);
		$asset->setDependencies($dependencies);
		$asset->setSlug($key);

		return $asset;
	}

	/**
	 * Returns the actual path to an asset based on the key provided,
	 * by resolving to the correct location in the correct theme.
	 *
	 * @param  string $key
	 * @return string
	 * @throws \InvalidArgumentException
	 */
	public function getPath($key)
	{
		// if it's an Http resource, let's go ahead and just return
		// the path as is.
		if ( Str::contains($key, ['http://', 'https://'])) {
			return $key;
		}

		list($section, $relativePath, $extension) = (new NamespacedItemResolver)->parseKey($key);

		// If we have a section, let's check if it's a namespace
		// or a package section
		if (isset($section))
		{
			$paths = $this->themes->getCascadedPackageAssetPaths($section);
		} else {
			// Otherwise we'll just find assets for the theme itself without
			// the categorization of a section

			$paths = $this->themes->getCascadedAssetPaths();
		}

		$file = false;

		// Now loop through the available paths to see if the asset
		// exists. If it does, we'll return the file.
		foreach ($paths as $path) {
			$file = rtrim($path, '/') . '/' . $relativePath . '.' . $extension;

			if (app()['files']->exists($file)) {
				return $file;
			}
		}

		return $file;
	}

	/**
	 * Get an asset's url
	 *
	 * @param $key
	 * @return mixed
	 */
	public function getUrl($key)
	{
		$path = $this->getPath($key);

		return $this->getAssetUrl($path);
	}

	/**
	 * Returns styles sorted by dependencies
	 *
	 * @return mixed
	 */
	public function getSortedStyles()
	{
		return $this->sortAssetsByType('styles');
	}

	/**
	 * Returns scripts sorted by dependencies
	 *
	 * @return mixed
	 */
	public function getSortedScripts()
	{
		return $this->sortAssetsByType('scripts');
	}

	/**
	 * Sort our attributes by type
	 *
	 * @param $type
	 */
	protected function sortAssetsByType($type)
	{
		if ( ! in_array($type, ['scripts', 'styles']))
		{
			throw new InvalidArgumentException("Cannot sort invalid type [{$type}]");
		}

		return (new Sorter($this->$type))->order()->all();
	}

	/**
	 * Compiles styles sorted by dependencies and returns
	 * the urls for each.
	 *
	 * @return array
	 */
	public function getCompiledStyles()
	{
		return $this->getCompiledAssetsByType('styles');
	}

	/**
	 * Compiles scripts sorted by dependencies and returns
	 * the urls for each.
	 *
	 * @return array
	 */
	public function getCompiledScripts()
	{
		return $this->getCompiledAssetsByType('scripts');
	}

	/**
	 * Compiles assets of the given type.
	 *
	 * @param $type
	 * @return array
	 */
	protected function getCompiledAssetsByType($type)
	{
		$assets = $this->sortAssetsByType($type);

		if ( $this->debug === true)
		{
			foreach ($assets as $asset)
			{
				if ($extension = pathinfo($asset->getSourcePath(), PATHINFO_EXTENSION))
				{
					foreach ($this->getFilterInstancesByExtension($extension) as $filter)
					{
						$asset->ensureFilter($filter);
					}
				}
			}
		}
		else
		{
			$collection = new AssetCollection($assets);

			foreach ($collection->all() as $asset)
			{
				if ($extension = pathinfo($asset->getSourcePath(), PATHINFO_EXTENSION))
				{
					foreach ($this->getFilterInstancesByExtension($extension) as $filter)
					{
						$asset->ensureFilter($filter);
					}
				}
			}

			$assets = array($collection);
		}

		$urls = [];

		$extension = $this->getCompiledExtension($type);

		foreach ($assets as $asset) {

			$filename = $this->getCompiledFilename($asset, $extension);

			$targetPathFull = $this->getCompiledTargetPath($type, $filename);
			$targetPathRelative = str_replace($this->compiledPath, '', $targetPathFull);

			$filesystem = $this->filesystem;

			if ( $this->forceCompile === true || ! $filesystem->exists($targetPathFull)) {

				if ( $filesystem->exists($targetPathFull)) {
					$filesystem->delete($targetPathFull);
				}

				// Asset writer needs the relative path, so we'll remove the
				// public path from our target path.
				$asset->setTargetPath($targetPathRelative);

				(new AssetWriter($this->compiledPath))->writeAsset($asset);
			}

			$urls[] = $this->getCompiledAssetUrl($targetPathRelative);
		}

		return $urls;
	}

	/**
	 * Returns the URL to the asset
	 *
	 * @param $targetPath
	 * @return mixed
	 */
	protected function getCompiledAssetUrl($targetPath)
	{
		$targetPath = str_replace($this->publicPath, '', $this->compiledPath) . $targetPath;

		return URL::asset($targetPath);
	}

	/**
	 * Returns the URL to the asset
	 *
	 * @param $targetPath
	 * @return mixed
	 */
	protected function getAssetUrl($targetPath)
	{
		$targetPath = str_replace($this->publicPath, '', $targetPath);

		return URL::asset($targetPath);
	}

	/**
	 * Returns our completed target path
	 *
	 * @param $type
	 * @param $filename
	 * @return string
	 */
	protected function getCompiledTargetPath($type, $filename)
	{
		$compilePath = $this->compiledPath;
		$typeDirectory = $this->getCompiledExtension($type);

		return "{$compilePath}/{$typeDirectory}/{$filename}";
	}

	/**
	 * Returns the unique filename for the compiled
	 * asset.
	 *
	 * @param AssetInterface $asset
	 * @param $extension
	 * @return string
	 */
	protected function getCompiledFilename(AssetInterface $asset, $extension)
	{
		$compileKey = '';

		if ( $asset instanceof AssetCollection) {

			$slug = $this->getAssetTypeFromExtension($extension);

			foreach ($asset->all() as $individual) {
				$compileKey .= $this->getCompiledKey($individual);
			}
		} else {
			$slug = $asset->getSlug();
			$extension = $this->getCompiledExtension($asset->getSourcePath());
			$compileKey = $this->getCompiledKey($asset);
		}

		$lastModified = $asset->getLastModified();
		$compileKey = md5($compileKey);

		return "{$slug}.{$compileKey}.{$lastModified}.{$extension}";
	}

	/**
	 * Generates our unique compiled file key. This will
	 * be used for caching and generating the filename.
	 *
	 * @param AssetInterface $asset
	 * @return string
	 */
	protected function getCompiledKey(AssetInterface $asset)
	{
		if ( ! $asset instanceof AssetInterface) {
			throw new InvalidArgumentException("Object must be an instance of AssetInterface to generate a compiled key.");
		}

		$key = $asset->getSourceRoot() . $asset->getSourcePath() . $asset->getTargetPath();

		foreach ($asset->getFilters() as $filter) {
			$key .= serialize($filter);
		}

		if ( $values = $asset->getValues()) {
			asort($values);

			$key .= serialize($values);
		}

		return $key;
	}

	/**
	 * Returns the type of asset based on the extension
	 * of the file in the given path.
	 *
	 * @param $path
	 * @return string
	 * @throws InvalidArgumentException
	 */
	public function getAssetTypeFromExtension($path)
	{
		$extension = pathinfo($path, PATHINFO_EXTENSION) ?: $path;

		if ( in_array($extension, $this->styleExtensions)) {
			return $this->compiledStylesFilename;
		} elseif ( in_array($extension, $this->scriptExtensions)) {
			return $this->compiledScriptsFilename;
		}

		throw new InvalidArgumentException("Cannot parse asset with extension [{$extension}]");
	}

	/**
	 * Parses our asset key to guess the type of asset
	 * that we are adding. Returns the class to use.
	 *
	 * @param $path
	 * @return string
	 */
	protected function getAssetClass($path)
	{
		if ( Str::contains($path, ['http://', 'https://'])) {
			$class = "Synergy\\Themes\\Assets\\Types\\HttpAsset";
		} else {
			$class = "Synergy\\Themes\\Assets\\Types\\FileAsset";
		}

		return $class;
	}

	/**
	 * Returns the folder for the asset type.
	 *
	 * @param $type
	 * @return string
	 */
	protected function getCompiledExtension($type)
	{
		$extension = pathinfo($type, PATHINFO_EXTENSION);

		if ( $type == 'styles' || in_array($extension, $this->styleExtensions)) {
			return 'css';
		} elseif ( $type == 'scripts' || in_array($extension, $this->scriptExtensions)) {
			return 'js';
		}

		throw new InvalidArgumentException("No compile extension defined for type [{$type}]");
	}

	/**
	 * Returns the Filter instances for the given extension
	 * type
	 *
	 * @param $extension
	 * @return mixed
	 */
	public function getFilterInstancesByExtension($extension)
	{
		$instances = [];
		$filters = array_get($this->filters, $extension, []);

		foreach ($filters as $filter) {
			$instances[] = new $filter;
		}

		return $instances;
	}

	/**
	 * Returns the array of queued style assets
	 *
	 * @return array
	 */
	public function getStyles()
	{
		return $this->styles;
	}

	/**
	 * Returns the array of queued script assets
	 *
	 * @return array
	 */
	public function getScripts()
	{
		return $this->scripts;
	}

	/**
	 * @return mixed
	 */
	public function getCompiledPath()
	{
		return $this->compiledPath;
	}

	/**
	 * @param mixed $compiledPath
	 */
	public function setCompiledPath($compiledPath)
	{
		$this->compiledPath = $compiledPath;
	}

	/**
	 * Returns the source path to our asset.
	 *
	 * @param $path
	 * @return string
	 */
	public function getSourcePath($path)
	{
		if ( Str::contains($path, ['http://', 'https://']))
		{
			return $path;
		}

		return $this->sourcePath . '/' . $path;
	}

	/**
	 * @param $sourcePath
	 * @return string
	 */
	public function setSourcePath($sourcePath)
	{
		return $this->sourcePath = $sourcePath;
	}

	/**
	 * @return string
	 */
	public function getPublicPath()
	{
		return $this->publicPath;
	}

	/**
	 * @param string $publicPath
	 */
	public function setPublicPath($publicPath)
	{
		$this->publicPath = $publicPath;
	}

	/**
	 * @return boolean
	 */
	public function isDebug()
	{
		return $this->debug;
	}

	/**
	 * @param boolean $debug
	 */
	public function setDebug($debug)
	{
		$this->debug = $debug;
	}

	/**
	 * Sets our debug status.
	 *
	 * If a debug is passed from the config, we will use the
	 * value provided. If the config value is set to null,
	 * we will attempt to get the current environment.
	 *
	 * @param null $debug
	 * @return bool|null
	 */
	public function guessDebug($debug = null)
	{
		if ( is_null($debug))
		{
			if ( app()->environment() == 'production')
			{
				return false;
			}

			return true;
		}

		return $debug;
	}

	/**
	 * @return boolean
	 */
	public function isForceCompile()
	{
		return $this->forceCompile;
	}

	/**
	 * @param boolean $forceCompile
	 */
	public function setForceCompile($forceCompile)
	{
		$this->forceCompile = $forceCompile;
	}

	/**
	 * @return array
	 */
	public function getStyleExtensions()
	{
		return $this->styleExtensions;
	}

	/**
	 * @param array $styleExtensions
	 */
	public function setStyleExtensions($styleExtensions)
	{
		$this->styleExtensions = $styleExtensions;
	}

	/**
	 * @return array
	 */
	public function getScriptExtensions()
	{
		return $this->scriptExtensions;
	}

	/**
	 * @param array $scriptExtensions
	 */
	public function setScriptExtensions($scriptExtensions)
	{
		$this->scriptExtensions = $scriptExtensions;
	}

	/**
	 * @return mixed
	 */
	public function getCompiledStylesFilename()
	{
		return $this->compiledStylesFilename;
	}

	/**
	 * @param mixed $compiledStylesFilename
	 */
	public function setCompiledStylesFilename($compiledStylesFilename)
	{
		$this->compiledStylesFilename = $compiledStylesFilename;
	}

	/**
	 * @return mixed
	 */
	public function getCompiledScriptsFilename()
	{
		return $this->compiledScriptsFilename;
	}

	/**
	 * @param mixed $compiledScriptsFilename
	 */
	public function setCompiledScriptsFilename($compiledScriptsFilename)
	{
		$this->compiledScriptsFilename = $compiledScriptsFilename;
	}

	/**
	 * @return array
	 */
	public function getFilters()
	{
		return $this->filters;
	}

	/**
	 * @param array $filters
	 */
	public function setFilters($filters)
	{
		$this->filters = $filters;
	}
}
