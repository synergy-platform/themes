<?php

namespace Synergy\Themes\Assets;

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/themes
 */

use RuntimeException;
use Synergy\Dependable\Dependable;

class Asset implements Dependable {

	/**
	 * @var
	 */
	protected $slug;

	/**
	 * @var
	 */
	protected $key;

	/**
	 * @var array
	 */
	protected $dependencies;

	/**
	 * Array containing our attributes.
	 *
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * Creates a new instance of our Asset
	 *
	 * @param $slug
	 * @param $key
	 * @param array $dependencies
	 * @param array $attributes
	 */
	public function __construct($slug, $key, array $dependencies = [], $attributes = [])
	{

		$this->slug = $slug;

		$this->key = $key;

		$this->dependencies = $dependencies;

		$this->attributes = $attributes;
	}

	/**
	 * Returns the unique slug for the item
	 *
	 * @return mixed
	 */
	public function getSlug()
	{
		return $this->slug;
	}

	/**
	 * Returns the unique key for the
	 * @return mixed
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * Returns an array of the item's dependencies
	 *
	 * @return mixed
	 */
	public function getDependencies()
	{
		return $this->dependencies;
	}

	/**
	 * Returns the extension's attributes.
	 *
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}

	/**
	 * @return string
	 */
	public function getPath()
	{
		return realpath($this->attributes['path']);
	}

	/**
	 * Magic method for returning object properties.
	 *
	 * @param $name
	 * @return mixed
	 */
	function __get($name)
	{
		$method =  'get' . studly_case($name);

		if ( method_exists($this, $method)) {
			return $this->$method;
		}

		if ( isset($this->attributes, $name)) {
			return $this->attributes[$name];
		}

		throw new RuntimeException("{$name} is not a valid attrbute for your asset.");
	}
}
