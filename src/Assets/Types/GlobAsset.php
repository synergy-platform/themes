<?php

namespace Synergy\Themes\Assets\Types;

use Assetic\Asset\GlobAsset as AsseticGlobAsset;
use Synergy\Dependable\Dependable;

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/themes
 */

class GlobAsset extends AsseticGlobAsset implements Dependable
{
	use AssetTrait;
}
