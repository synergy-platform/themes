<?php

namespace Synergy\Themes\Assets\Types;

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/themes
 */

trait AssetTrait {

	/**
	 * @var
	 */
	protected $path;

	/**
	 * @var
	 */
	protected $slug;

	/**
	 * @var
	 */
	protected $key;

	/**
	 * @var array
	 */
	protected $dependencies = [];

	/**
	 * Sets the type of asset
	 *
	 * @var string
	 */
	protected $type;

	/**
	 * @return mixed
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * @param mixed $path
	 */
	public function setPath($path)
	{
		$this->path = $path;
	}

	/**
	 * @return mixed
	 */
	public function getSlug()
	{
		return $this->slug;
	}

	/**
	 * @param mixed $slug
	 */
	public function setSlug($slug)
	{
		$this->slug = $slug;
	}

	/**
	 * @return mixed
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * @param mixed $key
	 */
	public function setKey($key)
	{
		$this->key = $key;
	}

	/**
	 * @return array
	 */
	public function getDependencies()
	{
		return $this->dependencies;
	}

	/**
	 * @param array $dependencies
	 */
	public function setDependencies($dependencies)
	{
		$this->dependencies = $dependencies;
	}

	/**
	 * Returns the asset type
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * Sets the asset type
	 *
	 * @param string $type
	 */
	public function setType($type = null)
	{
		if ( is_null($type)) {
			$type = pathinfo($this->path, PATHINFO_EXTENSION);
		}

		$this->type = $type;
	}
}
