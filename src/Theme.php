<?php

namespace Synergy\Themes;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

/**
 * Part of the Theme package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Theme
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergys/theme
 */

class Theme implements Arrayable, Jsonable
{

	/**
	 * @var Themes
	 */
	protected $themes;

	/**
	 * @var
	 */
	protected $path;

	/**
	 * @var
	 */
	protected $slug;

	/**
	 * @var array
	 */
	protected $attributes;


	/**
	 * Creates new instance of the Theme class
	 *
	 * @param Themes $themes
	 * @param        $path
	 * @param        $slug
	 * @param array  $attributes
	 */
	public function __construct(Themes $themes, $path, $slug, array $attributes)
	{
		$this->themes = $themes;
		$this->path = $path;
		$this->slug = $slug;
		$this->attributes = $attributes;
	}


	/**
	 * Returns the unique slug
	 *
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}

	/**
	 * Returns theme's description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->attributes['description'];
	}

	/**
	 * Returns theme's name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->attributes['name'];
	}

	/**
	 * Returns theme's version
	 *
	 * @return string
	 */
	public function getVersion()
	{
		return $this->attributes['version'];
	}

	/**
	 * Returns theme's author
	 *
	 * @return string
	 */
	public function getAuthor()
	{
		return $this->attributes['author'];
	}

	/**
	 * Returns the parent theme's slug
	 *
	 * @return mixed
	 */
	public function getParentSlug()
	{
		return array_get($this->attributes, 'parent');
	}

	/**
	 * Returns the theme's base path
	 *
	 * @return mixed
	 */
	public function getPath()
	{
		return $this->path;
	}


	/**
	 * Get the packages path for the theme.
	 *
	 * @param  string  $package
	 * @return string
	 */
	public function getPackagesPath($package = null)
	{
		$path = $this->path.'/'.$this->themes->getPackagesPath();

		if (! is_null($package)) {
			$path .= '/'.$package;
		}

		return $path;
	}

	/**
	 * Get the namespaces path for the theme.
	 *
	 * @param  string  $namespace
	 * @return string
	 */
	public function getNamespacesPath($namespace = null)
	{
		$path = $this->path.'/'.$this->themes->getNamespacesPath();
		if (! is_null($namespace)) {
			$path .= '/'.$namespace;
		}
		return $path;
	}
	/**
	 * Returns the path for a package.
	 *
	 * @param  string  $package
	 * @return string
	 */
	public function getPackagePath($package)
	{
		return $this->getPackagesPath($package);
	}

	/**
	 * Returns the path for a namespace.
	 *
	 * @param  string  $namespace
	 * @return string
	 */
	public function getNamespacePath($namespace)
	{
		return $this->getNamespacePath($namespace);
	}

	/**
	 * Returns the views path for the theme.
	 *
	 * @return string
	 */
	public function getViewsPath()
	{
		return $this->path.'/'.$this->themes->getViewsPath();
	}

	/**
	 * Returns the views path for a package.
	 *
	 * @param  string  $package
	 * @return string
	 */
	public function getPackageViewsPath($package)
	{
		return $this->getPackagesPath($package).'/'.$this->themes->getViewsPath();
	}

	/**
	 * Returns the views path for a namespace.
	 *
	 * @param  string  $namespace
	 * @return string
	 */
	public function getNamespaceViewsPath($namespace)
	{
		return $this->getNamespacesPath($namespace).'/'.$this->themes->getViewsPath();
	}

	/**
	 * Returns the assets path for the theme,
	 *
	 * @return string
	 */
	public function getAssetsPath()
	{
		return $this->path.'/'.$this->themes->getAssetsPath();
	}

	/**
	 * Returns the assets path for a package.
	 *
	 * @param  string  $package
	 * @return string
	 */
	public function getPackageAssetsPath($package)
	{
		return $this->getPackagesPath($package).'/'.$this->themes->getAssetsPath();
	}

	/**
	 * Returns the views path for a namespace,
	 *
	 * @param  string  $namespace
	 * @return string
	 */
	public function getNamespaceAssetsPath($namespace)
	{
		return $this->getNamespacesPath($namespace).'/'.$this->themes->getAssetsPath();
	}

	/**
	 * @return Themes
	 */
	public function getThemes()
	{
		return $this->themes;
	}


	/**
	 * @param Themes $themes
	 */
	public function setThemes($themes)
	{
		$this->themes = $themes;
	}

	/**
	 * @return array
	 */
	public function getAttributes()
	{
		return $this->attributes;
	}


	/**
	 * @param array $attributes
	 */
	public function setAttributes($attributes)
	{
		$this->attributes = $attributes;
	}

	/**
	 * Sets a given attribute for the extension.
	 *
	 * @param  string  $key
	 * @param  mixed  $value
	 * @return void
	 */
	public function setAttribute($key, $value)
	{
		$this->attributes[$key] = $value;
	}


	/**
	 * Get the instance as an array.
	 *
	 * @return array
	 */
	public function toArray()
	{
		// TODO: Implement toArray() method.
	}


	/**
	 * Convert the object to its JSON representation.
	 *
	 * @param  int $options
	 *
	 * @return string
	 */
	public function toJson($options = 0)
	{
		return json_encode($this->toArray(), $options);
	}

	/**
	 * Dynamically retrieve attributes on the object.
	 *
	 * @param  string  $key
	 * @return mixed
	 */
	public function __get($key)
	{
		return $this->getAttribute($key);
	}

	/**
	 * Dynamically set attributes on the object.
	 *
	 * @param  string  $key
	 * @param  mixed  $value
	 * @return void
	 */
	public function __set($key, $value)
	{
		$this->setAttribute($key, $value);
	}

	/**
	 * Determines if an attribute exists on the object.
	 *
	 * @param  string  $key
	 * @return boolean
	 */
	public function __isset($key)
	{
		return isset($this->attributes[$key]);
	}

	/**
	 * Unset an attribute on the object.
	 *
	 * @param  string  $key
	 * @return void
	 */
	public function __unset($key)
	{
		unset($this->attributes[$key]);
	}
}