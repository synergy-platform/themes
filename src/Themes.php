<?php

namespace Synergy\Themes;

use Illuminate\Support\Collection;
use InvalidArgumentException;
use RuntimeException;

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/themes
 */

class Themes extends Collection
{
	/**
	 * Instance of the Active theme
	 *
	 * @var string
	 */
	protected $active;

	/**
	 * Instance of the fallback theme
	 *
	 * @var string
	 */
	protected $fallback;

	/**
	 * Name of the theme class used to create new instances
	 * of registered themes.
	 *
	 * @var string
	 */
	protected $themeClass = 'Synergy\Themes\Theme';

	/**
	 * Rules used to validate a theme before registering it
	 *
	 * @var array
	 */
	protected $validationRules = [];

	/**
	 * Path to views relative to base theme path
	 *
	 * @var string
	 */
	protected $viewsPath = 'resources/views';

	/**
	 * Path to packages relative to base theme path
	 *
	 * @var string
	 */
	protected $packagesPath = 'packages';

	/**
	 * Path to namespaces relative to base theme path
	 *
	 * @var string
	 */
	protected $namespacesPath = 'namespaces';

	/**
	 * Path to assets relative to base theme path
	 *
	 * @var string
	 */
	protected $assetsPath = 'resources/assets';

	/**
	 * Finds and registers all modules found using the finder
	 */
	public function findAndRegister()
	{
		foreach(app('themes.finder')->find() as $theme)
		{
			$this->register($theme);
		}
	}

	/**
	 * Registers our module
	 *
	 * @param $theme
	 */
	public function register($theme)
	{
		$theme = $this->createInstance($theme);

		$this->items[$theme->getSlug()] = $theme;
	}

	/**
	 * Creates a new instance of our Theme
	 *
	 * @param $theme
	 * @return Theme
	 */
	protected function createInstance($theme)
	{
		$attributes = app('files')->getRequire($theme);

		$messages = $this->validForRegistering($attributes);

		if ($messages->isEmpty()) {

			$path = dirname($theme);

			$slug = $attributes['slug'];
			unset($attributes['slug']);

			return $this->createTheme($path, $slug, $attributes);
		}

		throw new InvalidArgumentException("Please verify the theme manifest {$theme} for correct attributes.");
	}

	/**
	 * Validates the module manifest file
	 *
	 * @param $manifest
	 */
	public function validForRegistering($manifest)
	{
		$validator = app('validator')->make($manifest, $this->validationRules);

		return $validator->errors();
	}

	/**
	 * Return array of validation rules
	 *
	 * @return array
	 */
	public function getValidationRules()
	{
		return $this->validationRules;
	}

	/**
	 * Set array of validation rules
	 *
	 * @param array $validationRules
	 */
	public function setValidationRules($validationRules)
	{
		$this->validationRules = $validationRules;
	}

	/**
	 * Return the active theme
	 *
	 * @return Theme
	 */
	public function getActive()
	{
		return $this->active;
	}

	/**
	 * Returns the active theme or throws an exception if
	 * an active theme has not been set.
	 *
	 * @return Theme
	 */
	public function getActiveOrFail()
	{
		if ( ! $active = $this->getActive()) {
			throw new RuntimeException(trans('synergy-themes::message.exception.no_active_theme'));
		}

		return $active;
	}

	/**
	 * Set the active theme instance
	 *
	 * @return Theme
	 */
	public function setActive($slug)
	{
		if ( ! $active = $this->get($slug)) {
			throw new RuntimeException(trans('synergy-themes::message.exception.theme_not_registered', ['theme' => $slug]));
		}

		$this->active = $active;
	}

	/**
	 * Returns the fallback theme
	 *
	 * @return Theme
	 */
	public function getFallback()
	{
		return $this->fallback;
	}

	/**
	 * Returns the active theme
	 *
	 * @param $fallback
	 *
	 * @return Theme
	 */
	public function setFallback($slug)
	{
		if ( ! $fallback = $this->get($slug)) {
			throw new RuntimeException(trans('synergy-themes::message.exception.theme_not_registered', ['slug' => $slug]));
		}

		$this->fallback = $fallback;
	}

	/**
	 * Creates a new theme instance
	 *
	 * @param $path
	 *
	 * @return Theme
	 */
	public function createTheme($path, $slug, $attributes)
	{
		$class = '\\'.ltrim($this->themeClass, '\\');

		return new $class($this, $path, $slug, $attributes);
	}

	/**
	 * Returns the name of the class used to register
	 * new theme instances.
	 *
	 * @return string
	 */
	public function getThemeClass()
	{
		return $this->themeClass;
	}


	/**
	 * Sets the name of the theme class to use.
	 *
	 * @param string $themeClass
	 */
	public function setThemeClass($themeClass)
	{
		$this->themeClass = $themeClass;
	}


	/**
	 * Returns views path relative to the base theme path.
	 *
	 * @return string
	 */
	public function getViewsPath()
	{
		return $this->viewsPath;
	}


	/**
	 * Sets the default views path relative to the base theme path.
	 *
	 * @param string $viewsPath
	 */
	public function setViewsPath($viewsPath)
	{
		$this->viewsPath = $viewsPath;
	}


	/**
	 * Returns packages path relative to the base theme path.
	 *
	 * @return string
	 */
	public function getPackagesPath()
	{
		return $this->packagesPath;
	}


	/**
	 * Sets the default packagespath relative to the base theme path.
	 *
	 * @param string $packagesPath
	 */
	public function setPackagesPath($packagesPath)
	{
		$this->packagesPath = $packagesPath;
	}


	/**
	 * Returns namespaces path relative to the base theme path.
	 *
	 * @return string
	 */
	public function getNamespacesPath()
	{
		return $this->namespacesPath;
	}


	/**
	 * Sets the default namespaces path relative to the base theme path.
	 *
	 * @param string $namespacesPath
	 */
	public function setNamespacesPath($namespacesPath)
	{
		$this->namespacesPath = $namespacesPath;
	}


	/**
	 * Returns assets path relative to the base theme path.
	 *
	 * @return string
	 */
	public function getAssetsPath()
	{
		return $this->assetsPath;
	}


	/**
	 * Sets the default assets path relative to the base theme path.
	 *
	 * @param string $assetsPath
	 */
	public function setAssetsPath($assetsPath)
	{
		$this->assetsPath = $assetsPath;
	}

	/**
	 * Returns the cascaded view paths for the active theme and all
	 * of it's parents right up to the fallback theme.
	 *
	 * @param  mixed  $theme
	 * @return array
	 */
	public function getCascadedViewPaths($theme = null)
	{
		return $this->getCascadedPaths('getViewsPath', null, $theme);
	}

	/**
	 * Returns the cascaded view paths for a package in the active theme
	 * and all of it's parents right up to the fallback theme.
	 *
	 * @param  string  $package
	 * @param  mixed   $theme
	 * @return array
	 */
	public function getCascadedPackageViewPaths($package, $theme = null)
	{
		return $this->getCascadedPaths('getPackageViewsPath', $package, $theme);
	}

	/**
	 * Returns the cascaded view paths for a namespace in the active theme
	 * and all of it's parents right up to the fallback theme.
	 *
	 * @param  string  $namespace
	 * @param  mixed   $theme
	 * @return array
	 */
	public function getCascadedNamespaceViewPaths($namespace, $theme = null)
	{
		return $this->getCascadedPaths('getNamespaceViewsPath', $namespace, $theme);
	}

	/**
	 * Returns the cascaded asset paths for the active theme and all
	 * of it's parents right up to the fallback theme.
	 *
	 * @param  mixed  $theme
	 * @return array
	 */
	public function getCascadedAssetPaths($theme = null)
	{
		return $this->getCascadedPaths('getAssetsPath', null, $theme);
	}

	/**
	 * Returns the cascaded asset paths for a package in the active theme
	 * and all of it's parents right up to the fallback theme.
	 *
	 * @param  string  $package
	 * @param  mixed   $theme
	 * @return array
	 */
	public function getCascadedPackageAssetPaths($package, $theme = null)
	{
		return $this->getCascadedPaths('getPackageAssetsPath', $package, $theme);
	}

	/**
	 * Returns the cascaded asset paths for a namespace in the active theme
	 * and all of it's parents right up to the fallback theme.
	 *
	 * @param  string  $namespace
	 * @param  mixed   $theme
	 * @return array
	 */
	public function getCascadedNamespaceAssetPaths($namespace, $theme = null)
	{
		return $this->getCascadedPaths('getNamespaceAssetsPath', $namespace, $theme);
	}

	/**
	 * Gets cascaded paths by calling a method on the active
	 * theme, it's parents and the fallback theme.
	 *
	 * @param  string  $method
	 * @param  string  $argument
	 * @param  mixed   $theme
	 * @return array   $paths
	 */
	protected function getCascadedPaths($method, $argument = null, $theme = null)
	{
		$paths  = array();
		$looped = array();

		$current = ! is_null($theme) ? $theme : $this->getActiveOrFail();

		while (true) {

			// Add the current "views path" to the array of paths
			$paths[]  = $current->$method($argument);
			$looped[] = $current;

			// If there is no parent theme, we will break our loop
			if (! $parentSlug = $current->getParentSlug()) {
				break;
			}

			// If the parent slug exists and we haven't registered
			// the theme, we will automatically register it now.
			if (! isset($this[$parentSlug])) {
				$this->register($parentSlug);
			}

			// If the parent is the fallback theme, break our loop
			// as we'll add it's path on at the end.
			if (($parent = $this[$parentSlug]) === $this->getFallback()) {
				break;
			}

			// Assign the current theme to the parent
			$current = $parent;
		}

		// If we have a fallback theme and we haven't looped through it (i.e.,
		// it is not the direct parent of any theme or is it the theme the
		// cascaded paths were requested for) we will append it's path to the
		// array of paths.
		if ($fallback = $this->getFallback() and ! in_array($fallback, $looped)) {
			$paths[] = $fallback->$method($argument);
		}

		return $paths;
	}
}
