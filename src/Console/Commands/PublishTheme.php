<?php

namespace Synergy\Themes\Console\Commands;

use Illuminate\Console\Command;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem as Flysystem;
use League\Flysystem\MountManager;
use Synergy\Modules\Module;

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://gitlab.com/synergy-platform/themes
 */


class PublishTheme extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:publish
    	{--module= : Slug of the module to publish}
    	{--all? : Flag to publish all modules}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publishes a theme\'s assets';

    /**
     * Instance of the Themes class
     *
     * @var \Synergy\Themes\Themes
     */
	protected $themes;

    /**
     * Instance of the Modules class
     *
     * @var \Synergy\Modules\Modules
     */
    protected $modules;

    /**
     * Array containing areas in the theme to
     * publish
     *
     * @var array
     */
    protected $areas = [
        'packages',
        'resources',
        'namespaces'
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $modules = $this->setModule();

		if (empty($modules)) {
            return $this->error('No modules were found.');
        }

        $themePath = $this->getThemeSourceDirectory();

        foreach($modules as $module) {
            $modulePath = $this->getModuleThemeSource($module);

            if (is_dir($modulePath)) {
                $this->publishDirectory($modulePath, $themePath);
            } else {
                $this->comment('Module ' . $module->getSlug() . ' does not contain any theme files.');
            }
        }
    }

    /**
     * Publish the given directory to the given source
     *
     * Credit to Taylor Otwell in Laravel's VendorPublishCommand
     *
     * @param $from
     * @param $to
     */
    protected function publishDirectory($from, $to)
    {
        $manager = new MountManager([
            'from' => new Flysystem(new Local(($from))),
            'to' => new Flysystem(new Local($to))
        ]);

        foreach ($manager->listContents('from://', true) as $file) {
            if ($file['type'] === 'file') {
                $manager->put('to://'.$file['path'], $manager->read('from://'.$file['path']));
            }
        }

        $this->status($from, $to, 'Directory');
    }

    /**
     * Returns the path to the module's theme directory
     *
     * @param \Synergy\Modules\Module $module
     *
     * @return string
     */
    protected function getModuleThemeSource(Module $module)
    {
        return $module->getPath() . DIRECTORY_SEPARATOR . 'themes';
    }

    /**
     * Returns path to the public themes directory
     *
     * @return string
     */
    protected function getThemeSourceDirectory()
    {
        return public_path('themes');
    }

    /**
     * @return array
     */
    public function setModule()
    {
        if ($module = app('modules')->get($this->option('module'))) {
            return [$module];
        }

        if ( $this->argument('all')) {
            return app('modules')->all();
        }

        return [];
    }

    /**
     * Write a status message to the console.
     *
     * @param  string  $from
     * @param  string  $to
     * @param  string  $type
     * @return void
     */
    protected function status($from, $to, $type)
    {
        $from = str_replace(base_path(), '', realpath($from));

        $to = str_replace(base_path(), '', realpath($to));

        $this->line('<info>Copied '.$type.'</info> <comment>['.$from.']</comment> <info>To</info> <comment>['.$to.']</comment>');
    }
}
