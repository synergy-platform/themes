<?php

namespace Synergy\Themes;

use Symfony\Component\Finder\Finder as SymfonyFinder;

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/themes
 */

class Finder
{

	/**
	 * Instance of the Symfony Finder
	 *
	 * @var SymfonyFinder
	 */
	protected $finder;

	/**
	 * Array containing paths to module storage locations
	 *
	 * @var array
	 */
	protected $paths = [];

	/**
	 * Path to the manifest file relative to the module's
	 * root path
	 *
	 * @var string
	 */
	protected $manifest = "theme.php";

	/**
	 * Max depth to search for manifest file
	 *
	 * @var int
	 */
	protected $depth = 2;


	/**
	 * Creates a new instance of the module finder class
	 *
	 * @param $paths
	 */
	function __construct(SymfonyFinder $finder, array $paths)
	{
		$this->finder = $finder;

		$this->paths = $paths;
	}

	/**
	 * Finds themes in the given paths
	 *
	 * @return array
	 */
	public function find()
	{
		$themes = [];

		foreach($this->paths as $path) {
			$themes = array_merge($themes, $this->findInPath($path));
		}

		return $themes;
	}


	/**
	 * @param $path
	 */
	public function findInPath($path)
	{
		$paths = [];

		if (is_dir($path)) {

			$themes = $this->finder
				->in($path)
				->name($this->manifest)
				->depth($this->depth)
				->followLinks();

			foreach ($themes as $module) {
				$paths[] = $module->getRealPath();
			}
		}

		return $paths;
	}


	/**
	 * Returns instance of the Symfony Finder
	 *
	 * @return mixed
	 */
	public function getFinder()
	{
		return $this->finder;
	}

	/**
	 * Returns array of module storage paths
	 *
	 * @return array
	 */
	public function getPaths()
	{
		return $this->paths;
	}


	/**
	 * Sets the array of module storage paths
	 *
	 * @param array $paths
	 */
	public function setPaths($paths)
	{
		$this->paths = $paths;
	}


	/**
	 * Returns the name of the manifest file
	 *
	 * @return string
	 */
	public function getManifest()
	{
		return $this->manifest;
	}


	/**
	 * Sets the name of the manifest file
	 *
	 * @param string $manifest
	 */
	public function setManifest($manifest)
	{
		$this->manifest = $manifest;
	}


	/**
	 * Returns the max depth to search for manifest file
	 *
	 * @return int
	 */
	public function getDepth()
	{
		return $this->depth;
	}


	/**
	 * Sets the max depth to search for manifest files
	 *
	 * @param int $depth
	 */
	public function setDepth($depth)
	{
		$this->depth = $depth;
	}
}
