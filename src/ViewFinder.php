<?php

namespace Synergy\Themes;

use Illuminate\Support\NamespacedItemResolver;
use Illuminate\View\FileViewFinder;
use InvalidArgumentException;

/**
 * Part of the Themes package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Themes
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/themes/themes
 */

class ViewFinder extends FileViewFinder
{

	/**
	 * Instance of the THemes class
	 *
	 * @var Themes
	 */
	protected $themes;

	/**
	 * Get the fullly qualified location of the view.
	 *
	 * @param  string  $name
	 * @return string
	 * @throws \InvalidArgumentException
	 */
	public function find($name)
	{
		$name = str_replace('.', '/', $name);

		// If the theme bag instance has not been set, we will just let
		// the default handler take control of loading the views.
		if (! isset($this->themes)) {
			return parent::find($name);
		}

		// Parse the name
		$resolver = new NamespacedItemResolver;

		list($section, $view) = $resolver->parseKey($name);

		try {
			// If we have a package listed, let's just check firstly
			// if it's actually referring to a hard-coded namespace.
			// Namespaces override packages in Themes, as they do in views.
			if (isset($section)) {

				if (isset($this->hints[$section])) {
					$sectionType = 'namespaces';
					$paths = $this->themes->getCascadedNamespaceViewPaths($section);
				} else {
					$sectionType = 'packages';
					$paths = $this->themes->getCascadedPackageViewPaths($section);
				}

				$view  = $this->findInPaths($view, $paths);
			} else {

				$paths = $this->themes->getCascadedViewPaths();
				$view  = $this->findInPaths($view, $paths);
			}
		}

		// We couldn't find the view using our theming system
		catch (InvalidArgumentException $e) {
			// Let's fallback to the normal view system.
			try {
				return parent::find($name);
			}
			// If we got an error with the normal view system, we'll
			// catch it here and manipulate the text, so that the user
			// knows we've been searching in the theme as well as the
			// normal view structure. Just a bit friendlier, yeah?
			catch (InvalidArgumentException $e) {
				// Grab the relevent themes from the theme bag
				$active   = $this->themes->getActive();
				$fallback = $this->themes->getFallback();

				// If we had a section, throw an Exception that's more aimed at
				// debugging why the package does not exist.
				if (isset($section)) {
					$message = sprintf(
						'Theme [%s] view [%s] could not be found in theme [%s]',
						$sectionType,
						$name,
						$active->getSlug()
					);
				} else {
					$message = sprintf(
						'Theme view [%s] could not be found in theme [%s]',
						$name,
						$active->getSlug()
					);
				}

				$message .= ($active->getParentSlug()) ? ' or any of its parent themes' : '';
				$message .= ($fallback and $fallback != $active) ? " or the fallback theme [{$fallback->getSlug()}]." : '.';
				$message .= ' The standard view finder has also failed to find the view.';

				throw new InvalidArgumentException($message);
			}
		}
		return $view;
	}

	/**
	 * Returns all the namespaces registered with the
	 * view finder.
	 *
	 * @return array
	 */
	public function getNamespaces()
	{
		return $this->hints;
	}

	/**
	 * Sets the theme bag instance on the view finder.
	 *
	 * @param  \Synergy\Themes\Themes  $themes
	 * @return void
	 */
	public function setThemes(Themes $themes)
	{
		$this->themes = $themes;
	}
}
